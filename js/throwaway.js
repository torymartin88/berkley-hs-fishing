/* Not for use in production.
	The code here is purely for convenience purposes */

$(function() {

	/* Allows expand and contract of challenge complete */
	
	$('.challenge .topbar span').click(function() {
		console.log($(this).parent().parent());
		$(this).parent().parent().toggleClass('complete');
	});
});