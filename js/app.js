/* Main App Javascript */


/* Init Foundation JS */
$(document).foundation();


$(function() {

	FastClick.attach(document.body);

	$('.js-close').click(function() {
		$('.user-details').slideToggle();
	});

});
