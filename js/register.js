/* Functionality for the Registration Page Only */


/* Populate the highschools variable to fill the autocomplete box for High Schools 
	Test/temp data below
*/
var highschools = ['Broad River School',
'Independence High',
'Deer Valley High',
'Golden Sierra College',
'Northride Academy',
'Green Meadows University',
'Winters Middle School',
'Greenlands University',
'Holy Oaks High',
'Northride High School',
'Winterville University',
'Timber Creek University',
'Grapevine Middle School',
'Mammoth High',
'Whale Gulch High',
'Silver Valley School',
'Woodside High School',
'Westview University',
'Granite Bay College',
'Apple Valley School',
'Deer Valley University',
'Mammoth School',
'Meadows School',
'Seal Coast Elementary',
'Granite Bay High',
'Central Academy',
'Whale Coast Middle School',
'Whale Coast College',
'Golden Oak University',
'Village College'];


$(function() {

	FastClick.attach(document.body);

	/* init highschool search */
	$('.highschool-search-main').typeahead({
		hint: true,
		highlight: true,
		minLength: 1
	},
	{
		name: 'highschools',
		displayKey: 'value',
		source: substringMatcher(highschools)
	});

	$noFacebook = $('.no-facebook');
	$facebookButton = $('.facebook-register-button');
	$registerButton = $('.register-button');

	$('.facebook-link a').on('click', facebookLoginFunc);


	/* check coach/student radio */
	$signupType = $("input[name='signupType']");
	$studentSpecific = $('.student-specific');
	$coachSpecific = $('.coach-specific');

	$signupType.on('change', function() {
		checkSignupType();
	});


	/* Check highschool radio */
	$schoolMember = $("input[name='schoolMember']");
	$highschoolSearch = $('.highschool-search');

	$schoolMember.on('change', function() {
		checkHighschoolRadio();
	});
})


var facebookLogin = false;


var facebookLoginFunc = function() {
	$this = $('.facebook-link a');
	$this.off('click');

	if (!facebookLogin) {
		$this.parent().html('You can also <a>Sign up with Email</a>');
		$noFacebook.slideUp();
		$facebookButton.css('display', 'block');
		$registerButton.css('display', 'none');
	} else {
		$this.parent().html('You can also <a>Sign up with Facebook</a>');
		$noFacebook.slideDown();
		$facebookButton.css('display', 'none');
		$registerButton.css('display', 'block');
	}

	$('.facebook-link a').on('click', facebookLoginFunc);

	facebookLogin = !facebookLogin;
};


var checkSignupType = function() {
	if ($("input[name='signupType']:checked").val() == 'Yes') {
		$studentSpecific.slideDown();
		$coachSpecific.slideUp();
		checkHighschoolRadio();
	} else {
		$studentSpecific.slideUp();
		$coachSpecific.slideDown();
		$highschoolSearch.slideDown();
	}
};


var checkHighschoolRadio = function() {
	if ($("input[name='schoolMember']:checked").val() == 'Yes')
		$highschoolSearch.slideDown();
	else 
		$highschoolSearch.slideUp();
};


var substringMatcher = function(strs) {
	return function findMatches(q, cb) {
		var matches, substrRegex;

		// an array that will be populated with substring matches
		matches = [];

		// regex used to determine if a string contains the substring `q`
		substrRegex = new RegExp(q, 'i');

		// iterate through the pool of strings and for any string that
		// contains the substring `q`, add it to the `matches` array
		$.each(strs, function(i, str) {
			if (substrRegex.test(str)) {
			// the typeahead jQuery plugin expects suggestions to a
			// JavaScript object, refer to typeahead docs for more info
				matches.push({ value: str });
			}
		});

		cb(matches);
	};
};
